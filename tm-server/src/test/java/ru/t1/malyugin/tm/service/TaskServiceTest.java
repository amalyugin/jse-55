package ru.t1.malyugin.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IProjectDTOService PROJECT_DTO_SERVICE = SERVICE_LOCATOR.getProjectDTOService();

    @NotNull
    private static final ITaskDTOService TASK_DTO_SERVICE = SERVICE_LOCATOR.getTaskDTOService();

    @NotNull
    private static final IUserDTOService USER_DTO_SERVICE = SERVICE_LOCATOR.getUserDTOService();

    @NotNull
    private static String userId1 = "";

    @NotNull
    private static String userId2 = "";

    @NotNull
    private static String projectId = "";

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        userId1 = USER_DTO_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL).getId();
        userId2 = USER_DTO_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL).getId();
        projectId = PROJECT_DTO_SERVICE.create(userId1, "TST_P", "TST_D").getId();
    }

    @AfterClass
    public static void tearDown() {
        PROJECT_DTO_SERVICE.removeById(projectId);
        USER_DTO_SERVICE.removeById(userId1);
        USER_DTO_SERVICE.removeById(userId2);
        CONNECTION_SERVICE.closeEntityManagerFactory();
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO("P" + i, "D" + i);
            if (i <= NUMBER_OF_TASKS / 2) task.setUserId(userId1);
            else task.setUserId(userId2);
            if (i <= 3) task.setProjectId(projectId);
            TASK_DTO_SERVICE.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_DTO_SERVICE.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testClear() {
        final int expected = 0;
        TASK_DTO_SERVICE.clear();
        Assert.assertEquals(expected, TASK_DTO_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        TASK_DTO_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_TASKS, TASK_DTO_SERVICE.getSize());
        TASK_DTO_SERVICE.clear(userId1);
        final int expected = 0;
        Assert.assertEquals(expected, TASK_DTO_SERVICE.getSize(userId1));
        Assert.assertNotEquals(expected, TASK_DTO_SERVICE.getSize(userId2));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(TASK_LIST.size(), TASK_DTO_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final long actualProjectList = TASK_DTO_SERVICE.getSize(userId1);
        Assert.assertEquals(actualProjectList, TASK_DTO_SERVICE.getSize(userId1));
        Assert.assertEquals(0, TASK_DTO_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.getSize(null));
    }


    @Test
    public void testCrateTaskDTO() {
        final int expectedNumberOfEntries = NUMBER_OF_TASKS + 2;
        @NotNull final TaskDTO task1 = TASK_DTO_SERVICE.create(userId1, "NAME1", "DESCRIPTION");
        @NotNull final TaskDTO task2 = TASK_DTO_SERVICE.create(userId2, "NAME2", null);

        Assert.assertEquals(expectedNumberOfEntries, TASK_DTO_SERVICE.getSize());
        Assert.assertEquals("NAME1", task1.getName());
        Assert.assertEquals("DESCRIPTION", task1.getDescription());
        Assert.assertEquals(userId1, task1.getUserId());

        Assert.assertEquals("NAME2", task2.getName());
        Assert.assertEquals("", task2.getDescription());
        Assert.assertEquals(userId2, task2.getUserId());
    }

    @Test
    public void testCrateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.create(null, UNKNOWN_ID, null));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_DTO_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_DTO_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() {
        @NotNull final TaskDTO task = TASK_DTO_SERVICE.findAll(userId1).get(0);
        @NotNull final String test = "TEST";
        TASK_DTO_SERVICE.updateById(userId1, task.getId(), test, null);
        TASK_DTO_SERVICE.updateById(userId1, task.getId(), test, test);

        @Nullable final TaskDTO actualTaskDTO = TASK_DTO_SERVICE.findOneById(userId1, task.getId());
        Assert.assertNotNull(actualTaskDTO);
        Assert.assertEquals(test, actualTaskDTO.getName());
        Assert.assertEquals(test, actualTaskDTO.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_DTO_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_DTO_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_DTO_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, null, UNKNOWN_ID));
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final TaskDTO task = TASK_DTO_SERVICE.findAll(userId1).get(0);
        TASK_DTO_SERVICE.changeStatusById(userId1, task.getId(), Status.COMPLETED);
        @Nullable TaskDTO actualTaskDTO = TASK_DTO_SERVICE.findOneById(userId1, task.getId());
        Assert.assertNotNull(actualTaskDTO);
        Assert.assertEquals(Status.COMPLETED, actualTaskDTO.getStatus());
        TASK_DTO_SERVICE.changeStatusById(userId1, task.getId(), null);
        actualTaskDTO = TASK_DTO_SERVICE.findOneById(userId1, task.getId());
        Assert.assertNotNull(actualTaskDTO);
        Assert.assertEquals(Status.COMPLETED, actualTaskDTO.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.changeStatusById(null, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testBindTaskToProjectDTO() {
        final String taskId = TASK_LIST.stream().filter(p -> p.getUserId().equals(userId1)).collect(Collectors.toList()).get(4).getId();
        final int actualNumberOfEntries = TASK_DTO_SERVICE.findAllByProjectId(userId1, projectId).size();
        final int expectedNumberOfEntries = actualNumberOfEntries + 1;
        TASK_DTO_SERVICE.bindTaskToProject(userId1, taskId, projectId);
        Assert.assertEquals(expectedNumberOfEntries, TASK_DTO_SERVICE.findAllByProjectId(userId1, projectId).size());
        @Nullable final TaskDTO task = TASK_DTO_SERVICE.findOneById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void testUnbindTaskToProjectDTO() {
        final String taskId = TASK_LIST.stream().filter(p -> p.getUserId().equals(userId1)).collect(Collectors.toList()).get(0).getId();
        final int actualNumberOfEntries = TASK_DTO_SERVICE.findAllByProjectId(userId1, projectId).size();
        final int expectedNumberOfEntries = actualNumberOfEntries - 1;
        TASK_DTO_SERVICE.unbindTaskFromProject(userId1, taskId, projectId);
        Assert.assertEquals(expectedNumberOfEntries, TASK_DTO_SERVICE.findAllByProjectId(userId1, projectId).size());
        Assert.assertNull(TASK_DTO_SERVICE.findOneById(taskId).getProjectId());
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<TaskDTO> actualTaskList = TASK_DTO_SERVICE.findAllByProjectId(userId1, projectId);
        @NotNull final List<TaskDTO> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> p.getProjectId() != null)
                .filter(p -> StringUtils.equals(projectId, p.getProjectId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList.size(), actualTaskList.size());
        for (int i = 0; i < expectedTaskList.size(); i++) {
            Assert.assertEquals(expectedTaskList.get(i).getId(), actualTaskList.get(i).getId());
        }
        Assert.assertEquals(Collections.emptyList(), TASK_DTO_SERVICE.findAllByProjectId(userId1, null));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_DTO_SERVICE.findAllByProjectId(null, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<TaskDTO> actualUserTaskList = TASK_DTO_SERVICE.findAll(userId1);
        int expectedNumberOfEntries = actualUserTaskList.size();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            expectedNumberOfEntries--;
            @NotNull final TaskDTO task = actualUserTaskList.get(i);
            TASK_DTO_SERVICE.removeById(userId1, task.getId());
            Assert.assertEquals(expectedNumberOfEntries, TASK_DTO_SERVICE.getSize(userId1));
        }
    }
*/
}