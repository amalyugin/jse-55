package ru.t1.malyugin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.api.service.model.ITaskService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.response.task.*;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.server.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private ITaskDTOService taskDTOService;

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        try {
            taskDTOService.changeStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskDTOService.changeStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskDTOService.changeStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            taskDTOService.updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskService.removeById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse creteTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task;
        try {
            task = taskDTOService.create(userId, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        try {
            taskService.clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task;
        try {
            task = taskDTOService.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListResponse showTaskList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowListRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<TaskDTO> taskList;
        try {
            taskList = taskDTOService.findAll(userId, sort);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowListResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListByProjectIdResponse showTaskListByProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowListByProjectIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<TaskDTO> taskList;
        try {
            taskList = taskDTOService.findAllByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowListByProjectIdResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        checkPermission(request);
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        try {
            taskService.bindTaskToProject(userId, taskId, projectId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindToProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        try {
            taskService.unbindTaskFromProject(userId, taskId, projectId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUnbindToProjectResponse();
    }

}