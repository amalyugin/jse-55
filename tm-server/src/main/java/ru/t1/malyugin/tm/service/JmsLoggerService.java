package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.component.JmsComponent;
import ru.t1.malyugin.tm.log.OperationEvent;

import javax.jms.TextMessage;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public final class JmsLoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final JmsComponent jmsComponent;

    @Autowired
    public JmsLoggerService(@NotNull final JmsComponent jmsComponent) {
        this.jmsComponent = jmsComponent;
    }

    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    private void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

    @SneakyThrows
    private void send(@NotNull final String text) {
        @NotNull final TextMessage message = jmsComponent.getSession().createTextMessage(text);
        jmsComponent.getMessageProducer().send(message);
    }

}