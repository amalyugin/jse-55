package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ISchemeService {

    void dropScheme(@Nullable String initToken);

    void updateScheme(@Nullable String initToken);

}