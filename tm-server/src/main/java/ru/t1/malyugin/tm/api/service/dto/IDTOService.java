package ru.t1.malyugin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;

import java.util.List;

public interface IDTOService<M extends AbstractDTOModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

    long getSize();

    void clear();

    @Nullable
    M findOneById(@Nullable String id);

    void removeById(@Nullable String id);

    @NotNull
    List<M> findAll();

}