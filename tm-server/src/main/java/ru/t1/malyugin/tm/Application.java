package ru.t1.malyugin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.t1.malyugin.tm.component.Bootstrap;
import ru.t1.malyugin.tm.configuration.ServerConfiguration;

@ComponentScan()
public final class Application {

    public static void main(@Nullable final String... args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}