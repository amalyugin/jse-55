package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.dto.IWBSDTORepository;
import ru.t1.malyugin.tm.api.service.dto.IWBSDTOService;
import ru.t1.malyugin.tm.comparator.WBSComparatorFunctional;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractWBSDTOService<M extends AbstractWBSDTOModel> extends AbstractUserOwnedDTOService<M>
        implements IWBSDTOService<M> {

    @NotNull
    @Override
    protected abstract IWBSDTORepository<M> getRepository();

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) {
        if (sort == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final IWBSDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (sort.getComparator() == WBSComparatorFunctional.CREATED)
                return repository.findAllOrderByCreated(userId);
            if (sort.getComparator() == WBSComparatorFunctional.NAME) return repository.findAllOrderByName(userId);
            if (sort.getComparator() == WBSComparatorFunctional.STATUS) return repository.findAllOrderByStatus(userId);
            return findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (comparator == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final IWBSDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (comparator == WBSComparatorFunctional.CREATED) return repository.findAllOrderByCreated(userId);
            if (comparator == WBSComparatorFunctional.NAME) return repository.findAllOrderByName(userId);
            if (comparator == WBSComparatorFunctional.STATUS) return repository.findAllOrderByStatus(userId);
            return findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return;
        if (status != null) model.setStatus(status);
        update(userId, model);
    }

}