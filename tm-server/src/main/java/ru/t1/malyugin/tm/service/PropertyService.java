package ru.t1.malyugin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    private static final String APPLICATION_NAME_DEFAULT = "tm";

    @NotNull
    private static final String APPLICATION_LOG_DIR_KEY = "application.log";

    @NotNull
    private static final String APPLICATION_LOG_DIR_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_DUMP_DIR_KEY = "application.dump";

    @NotNull
    private static final String APPLICATION_DUMP_DIR_DEFAULT = "./";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "9327328668";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "5455";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "76534";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String GIT_BRANCH = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_KEY = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "none";

    @NotNull
    private static final String DATABASE_CACHE_USE_SECOND_LEVEL_KEY = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_SECOND_LEVEL_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_CACHE_USE_QUERY_KEY = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_QUERY_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_KEY = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT = "false";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_KEY = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_DEFAULT = "tm";

    @NotNull
    private static final String DATABASE_PROVIDER_PATH_KEY = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_PROVIDER_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS_KEY = "database.cache.region.factory_class";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String DATABASE_LIQUIBASE_CONFIG = "database.liquibase_config";

    @NotNull
    private static final String DATABASE_LIQUIBASE_CONFIG_DEFAULT = "database.liquibase_config";

    @NotNull
    private static final String DATABASE_INIT_TOKEN_KEY = "database.init_token";

    @NotNull
    private static final String JMS_LOGGER_QUEUE_KEY = "jms.logger_queue";

    @NotNull
    private static final String JMS_URL_KEY = "jms.url";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean isExistsExternalConfig = isExistsExternalConfig();
        if (isExistsExternalConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationLogDir() {
        return getStringValue(APPLICATION_LOG_DIR_KEY, APPLICATION_LOG_DIR_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return readManifest(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return readManifest(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return readManifest(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return readManifest(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return readManifest(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return readManifest(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return readManifest(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return readManifest(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return readManifest(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getApplicationDumpDir() {
        return getStringValue(APPLICATION_DUMP_DIR_KEY, APPLICATION_DUMP_DIR_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        if (StringUtils.isNumeric(value)) return Integer.parseInt(value);
        else return Integer.parseInt(SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getJmsLoggerQueue() {
        return getStringValue(JMS_LOGGER_QUEUE_KEY);
    }

    @NotNull
    @Override
    public String getJmsLoggerUrl() {
        return getStringValue(JMS_URL_KEY);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
        if (StringUtils.isNumeric(value)) return Integer.parseInt(value);
        else return Integer.parseInt(SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getInitToken() {
        return getStringValue(DATABASE_INIT_TOKEN_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUsername() {
        return getStringValue(DATABASE_USERNAME_KEY);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_PASSWORD_KEY);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL_KEY);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER_KEY);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBFormatSQL() {
        return getStringValue(DATABASE_FORMAT_SQL_KEY, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSQL() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHmb2DDLAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheUseSecondLevel() {
        return getStringValue(DATABASE_CACHE_USE_SECOND_LEVEL_KEY, DATABASE_CACHE_USE_SECOND_LEVEL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheUseQueryCache() {
        return getStringValue(DATABASE_CACHE_USE_QUERY_KEY, DATABASE_CACHE_USE_QUERY_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheUseMinimalPuts() {
        return getStringValue(DATABASE_CACHE_USE_MINIMAL_PUTS_KEY, DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegionPrefix() {
        return getStringValue(DATABASE_CACHE_REGION_PREFIX_KEY, DATABASE_CACHE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheProviderFile() {
        return getStringValue(DATABASE_PROVIDER_PATH_KEY, DATABASE_PROVIDER_PATH_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegionFactoryClass() {
        return getStringValue(DATABASE_REGION_FACTORY_CLASS_KEY, DATABASE_REGION_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getLiquibaseConfig() {
        return getStringValue(DATABASE_LIQUIBASE_CONFIG, DATABASE_LIQUIBASE_CONFIG_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        if (StringUtils.isNumeric(value)) return Integer.parseInt(value);
        else return Integer.parseInt(PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = Files.newInputStream(file.toPath());
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    private String getEnvironmentKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvironmentKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String readManifest(@Nullable final String key) {
        if (StringUtils.isBlank(key)) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}