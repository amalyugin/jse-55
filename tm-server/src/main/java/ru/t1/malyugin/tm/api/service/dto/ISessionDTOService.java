package ru.t1.malyugin.tm.api.service.dto;

import ru.t1.malyugin.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

}