package ru.t1.malyugin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.model.AbstractWBSModel;

import java.util.List;

public interface IWBSRepository<M extends AbstractWBSModel> extends IUserOwnedRepository<M> {

    @NotNull
    List<M> findAllOrderByCreated(@NotNull String userId);

    @NotNull
    List<M> findAllOrderByName(@NotNull String userId);

    @NotNull
    List<M> findAllOrderByStatus(@NotNull String userId);

}