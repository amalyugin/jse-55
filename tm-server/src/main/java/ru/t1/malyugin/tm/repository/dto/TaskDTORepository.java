package ru.t1.malyugin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.malyugin.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDTORepository extends AbstractWBSDTORepository<TaskDTO> implements ITaskDTORepository {

    @NotNull
    @Override
    public Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}