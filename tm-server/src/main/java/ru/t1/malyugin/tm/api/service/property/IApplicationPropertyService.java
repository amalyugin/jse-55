package ru.t1.malyugin.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IApplicationPropertyService {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationDumpDir();

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationLogDir();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

}
