# TASK MANGER

## DEVELOPER INFO

* **Name**: Alexej Malyugin
* **E-mail**: amalyugin@t1-consulting.ru
* **E-mail**: malyugin.lesha@yandex.ru

## SOFTWARE

* **OS**: Windows 10
* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: Core i5-1135G7
* **RAM**: 16 GB
* **SSD**: 512 GB

## PROGRAM BUILD

```console
mvn clean install
```

## PROGRAM RUN

```console
java -jar ./task-manager.jar
```