package ru.t1.malyugin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractDTOModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(unique = true)
    private String login;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "locked_flg")
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return String.format("USER %s: %s", id, login);
    }

}