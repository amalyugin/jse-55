package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.*;

@Component
@NoArgsConstructor
public final class LoggerComponent {

    @NotNull
    private final static String QUEUE_NAME_KEY = "QUEUE_NAME";

    @NotNull
    private final static String QUEUE_NAME_DEFAULT = "TM_LOGGER";

    @Autowired
    public LoggerComponent(
            @NotNull final MessageListener entityListener,
            @NotNull final ConnectionFactory connectionFactory
    ) throws JMSException {
        @NotNull String queue = (System.getenv().containsKey(QUEUE_NAME_KEY)) ? System.getenv(QUEUE_NAME_KEY) : QUEUE_NAME_DEFAULT;
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(queue);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }
}