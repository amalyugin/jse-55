package ru.t1.malyugin.tm.configuration;

import com.mongodb.MongoClient;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.malyugin.tm")
public class LoggerConfiguration {

    @NotNull
    private final static String QUEUE_URL_KEY = "QUEUE_URL";

    @NotNull
    private final static String MONGO_HOST_KEY = "MONGO_HOST";

    @NotNull
    private final static String MONGO_PORT_KEY = "MONGO_PORT";

    @NotNull
    private final static String MONGO_HOST_DEFAULT = "localhost";

    @NotNull
    private final static String MONGO_PORT_DEFAULT = "27017";

    @Bean
    public ConnectionFactory factory() {
        @NotNull final String url = (System.getenv().containsKey(QUEUE_URL_KEY)) ? System.getenv(QUEUE_URL_KEY) : ActiveMQConnectionFactory.DEFAULT_BROKER_URL;
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        return connectionFactory;
    }

    @Bean
    @Scope("prototype")
    public MongoClient mongoClient() {
        @NotNull String host = (System.getenv().containsKey(MONGO_HOST_KEY)) ? System.getenv(MONGO_HOST_KEY) : MONGO_HOST_DEFAULT;
        @NotNull String port = (System.getenv().containsKey(MONGO_PORT_KEY)) ? System.getenv(MONGO_PORT_KEY) : MONGO_PORT_DEFAULT;
        return new MongoClient(host, Integer.parseInt(port));
    }

}