package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class Bootstrap {

    private void prepareStartup() {
        BasicConfigurator.configure();
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}