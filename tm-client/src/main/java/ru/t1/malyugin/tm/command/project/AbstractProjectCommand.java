package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.enumerated.Role;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println(project);
    }

    protected void renderProjectList(@Nullable final List<ProjectDTO> projects) {
        int index = 1;
        if (projects == null) return;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}