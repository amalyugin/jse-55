package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @NotNull final String name = command.getName();
        @Nullable final String argument = command.getArgument();
        if (!StringUtils.isBlank(name)) mapByName.put(name.trim(), command);
        if (!StringUtils.isBlank(argument)) mapByArgument.put(argument.trim(), command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument.trim());
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return mapByName.get(name.trim());
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return mapByName.values();
    }

}