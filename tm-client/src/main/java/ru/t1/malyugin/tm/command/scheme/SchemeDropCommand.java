package ru.t1.malyugin.tm.command.scheme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeDropRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class SchemeDropCommand extends AbstractSchemeCommand {

    @NotNull
    private static final String NAME = "scheme-drop";

    @NotNull
    private static final String DESCRIPTION = "Drop scheme";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[SCHEME DROP]");
        System.out.println("ENTER INIT TOKEN: ");
        @NotNull final String initToken = TerminalUtil.nextLine();
        @NotNull final SchemeDropRequest request = new SchemeDropRequest(initToken);
        schemeEndpoint.dropScheme(request);
    }

}