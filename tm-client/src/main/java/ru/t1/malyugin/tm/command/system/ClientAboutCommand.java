package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class ClientAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "about-client";

    @NotNull
    private static final String DESCRIPTION = "Client about";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLIENT INFORMATION]");
        System.out.println("NAME: " + propertyService.getApplicationName());
        System.out.println("AUTHOR NAME: " + propertyService.getAuthorName());
        System.out.println("AUTHOR E-MAIL: " + propertyService.getAuthorEmail());
    }

}