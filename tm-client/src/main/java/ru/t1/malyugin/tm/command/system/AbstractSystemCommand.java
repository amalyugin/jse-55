package ru.t1.malyugin.tm.command.system;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}