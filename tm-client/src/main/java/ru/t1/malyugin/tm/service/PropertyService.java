package ru.t1.malyugin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.IPropertyService;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    private static final String APPLICATION_LOG_DIR_KEY = "application.log";

    @NotNull
    private static final String APPLICATION_COMMAND_DIR_KEY = "application.command_scanner";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String TEST_ADMIN_LOGIN_KEY = "admin.login";

    @NotNull
    private static final String TEST_ADMIN_PASS_KEY = "admin.pass";

    @NotNull
    private static final String TEST_SOAP_LOGIN_KEY = "soap_user.login";

    @NotNull
    private static final String TEST_SOAP_PASS_KEY = "soap_user.pass";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String APPLICATION_NAME_DEFAULT = "tm_client";

    @NotNull
    private static final String APPLICATION_LOG_DIR_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_COMMAND_DIR_DEFAULT = "./";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean isExistsExternalConfig = isExistsExternalConfig();
        if (isExistsExternalConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationLogDir() {
        return getStringValue(APPLICATION_LOG_DIR_KEY, APPLICATION_LOG_DIR_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return readManifest(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return readManifest(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return readManifest(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getApplicationCommandScannerDir() {
        return getStringValue(APPLICATION_COMMAND_DIR_KEY, APPLICATION_COMMAND_DIR_DEFAULT);
    }

    @NotNull
    @Override
    public String getTestAdminLogin() {
        return getStringValue(TEST_ADMIN_LOGIN_KEY);
    }

    @NotNull
    @Override
    public String getTestAdminPassword() {
        return getStringValue(TEST_ADMIN_PASS_KEY);
    }

    @NotNull
    @Override
    public String getTestSoapLogin() {
        return getStringValue(TEST_SOAP_LOGIN_KEY);
    }

    @NotNull
    @Override
    public String getTestSoapPass() {
        return getStringValue(TEST_SOAP_PASS_KEY);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = Files.newInputStream(file.toPath());
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    private String getEnvironmentKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvironmentKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String readManifest(@Nullable final String key) {
        if (StringUtils.isBlank(key)) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

}