package ru.t1.malyugin.tm.command.user;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.user.UserRegistryRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-registry";

    @NotNull
    private static final String DESCRIPTION = "registry new user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");

        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final String token = authEndpoint.registry(request).getToken();
        setToken(token);
    }

}