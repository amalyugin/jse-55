package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.response.system.ServerInfoResponse;

@Component
public final class ServerInfoCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "info-server";

    @NotNull
    private static final String DESCRIPTION = "Show server info";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");

        @NotNull final ServerInfoRequest request = new ServerInfoRequest();
        @NotNull final ServerInfoResponse response = systemEndpoint.getInfo(request);
        @Nullable final Integer processorsCount = response.getProcessorsCount();
        @Nullable final String maxMemory = response.getMaxMemory();
        @Nullable final String totalMemory = response.getTotalMemory();
        @Nullable final String freeMemory = response.getFreeMemory();
        @Nullable final String usedMemory = response.getUsedMemory();

        System.out.println("PROCESSORS: " + processorsCount);
        System.out.println("MAX MEMORY: " + maxMemory);
        System.out.println("TOTAL MEMORY: " + totalMemory);
        System.out.println("FREE MEMORY: " + freeMemory);
        System.out.println("USED MEMORY: " + usedMemory);
    }

}