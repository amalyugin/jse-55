package ru.t1.malyugin.tm.exception.server;

import ru.t1.malyugin.tm.exception.AbstractException;

public final class ConnectionException extends AbstractException {

    public ConnectionException() {
        super("You are not connected to server...");
    }
}