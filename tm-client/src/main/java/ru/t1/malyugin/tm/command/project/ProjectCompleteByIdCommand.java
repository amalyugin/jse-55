package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete project by id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken(), id);
        projectEndpoint.completeProjectById(request);
    }

}