package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove project by id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        projectEndpoint.removeProjectById(request);
    }

}